const Product = require("./classes/Product");
const ShoppingCart = require("./classes/ShoppingCart");

const d = require("./classes/Discount");
const Discount = d.Discount;
const DiscountType = d.DiscountType;

describe('Shopping cart', () => {
    const cart = new ShoppingCart();

    afterEach(() => {
        cart.reset();
    });

    test('can add products', () => {
        cart.addProduct(new Product(1, 'P1', 1), 1);
        cart.addProduct(new Product(2, 'P2', 2), 1);

        expect(cart.itemCount()).toBe(2);
    });

    test('merges products', () => {
        const product = new Product(1, 'P1', 2);

        cart.addProduct(product, 3);
        cart.addProduct(product, 6);

        expect(cart.itemCount()).toBe(1);
    });

    test('updates product count', () => {
        const product = new Product(1, 'P1', 2);

        cart.addProduct(product, 1);

        expect(cart.getItem(0).count).toBe(1);

        cart.updateCount(product, 5);

        expect(cart.getItem(0).count).toBe(5);
    });

    test('removes product', () => {
        const product1 = new Product(1, 'P1', 1);
        const product2 = new Product(2, 'P2', 4);

        cart.addProduct(product1, 1);
        cart.addProduct(product2, 5);

        cart.removeProduct(product1);

        expect(cart.itemCount()).toBe(1);
    });

    test('calculates total', () => {
        const product1 = new Product(1, 'P1', 1);
        const product2 = new Product(2, 'P2', 4);

        cart.addProduct(product1, 1);
        cart.addProduct(product2, 5);

        expect(cart.getTotal()).toBe(21);
    });

    test('handles value off discount', () => {
        const product1 = new Product(1, 'P1', 1);
        const product2 = new Product(2, 'P2', 4);

        cart.addProduct(product1, 1);
        cart.addProduct(product2, 5);

        cart.setDiscount(new Discount('value_off', 3));

        expect(cart.getTotal()).toBe(18);
    });

    test('handles % off discount', () => {
        const product1 = new Product(1, 'P1', 1);
        const product2 = new Product(2, 'P2', 4);

        cart.addProduct(product1, 1);
        cart.addProduct(product2, 5);

        cart.setDiscount(new Discount('value_off_percentage', 10));

        expect(cart.getTotal()).toBe(18.9);
    });

    test('handles product level value off discount', () => {
        const product1 = new Product(1, 'P1', 4, new Discount(DiscountType.VALUE_OFF, 2));
        const product2 = new Product(2, 'P2', 7, new Discount(DiscountType.VALUE_OFF, 1));

        cart.addProduct(product1, 6);
        cart.addProduct(product2, 5);

        expect(cart.getTotal()).toBe(42);
    });

    test('handles product level % value off discount', () => {
        const product1 = new Product(1, 'P1', 4, new Discount(DiscountType.VALUE_OFF_PERCENTAGE, 12));
        const product2 = new Product(2, 'P2', 7, new Discount(DiscountType.VALUE_OFF_PERCENTAGE, 10));

        cart.addProduct(product1, 6);
        cart.addProduct(product2, 5);

        expect(cart.getTotal()).toBe(52.62);
    });

    test('handles product level new price discount', () => {
        const product1 = new Product(1, 'P1', 4, new Discount(DiscountType.CONDITIONAL_NEW_PRICE, undefined, 3, 5));
        const product2 = new Product(2, 'P2', 7, new Discount(DiscountType.CONDITIONAL_NEW_PRICE, undefined, 5, 15));

        cart.addProduct(product1, 6);
        cart.addProduct(product2, 5);

        expect(cart.getTotal()).toBe(53);
    });

    test('handles individual product discounts', () => {
        const product1 = new Product(1, 'P1', 4, new Discount(DiscountType.VALUE_OFF, 2));
        const product2 = new Product(2, 'P2', 4, new Discount(DiscountType.VALUE_OFF_PERCENTAGE, 12));
        const product3 = new Product(3, 'P3', 4, new Discount(DiscountType.CONDITIONAL_NEW_PRICE, undefined, 3, 5));

        cart.addProduct(product1, 5);
        cart.addProduct(product2, 6);
        cart.addProduct(product3, 7);

        expect(cart.getTotal()).toBe(52.12);
    });

    test('handles individual product discounts combined with total discount', () => {
        const product1 = new Product(1, 'P1', 4, new Discount(DiscountType.VALUE_OFF, 2));
        const product2 = new Product(2, 'P2', 4, new Discount(DiscountType.VALUE_OFF_PERCENTAGE, 12));
        const product3 = new Product(3, 'P3', 4, new Discount(DiscountType.CONDITIONAL_NEW_PRICE, undefined, 3, 5));

        cart.addProduct(product1, 5);
        cart.addProduct(product2, 6);
        cart.addProduct(product3, 7);

        cart.setDiscount(new Discount('value_off', 2.12))

        expect(cart.getTotal()).toBe(50);
    });
})
