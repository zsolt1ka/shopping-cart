# Shopping Cart - Description in progress

## About
The goal of this dojo is to implement a shopping cart with basic functionalities. The job is considered done if every test passes. 

## Requirements
The skeleton is implemented, but contains some TODOs, during the DOJO those TODOs should be replaced with implementation in `ShoppingCart.js` and the `CartItem.js`

### Functional requirements
**ShoppingCart.js**

**Add a product**  to the cart with count multiplier. If the product already exists in the cart increase the count of the cart item.
**Update count** of a product in the cart.
**Remove product** from the cart. 
**Apply a discount** on cart level of the following types:
- `VALUE_OFF` - means the total of the cart will be discounted with the provided value
- `VALUE_OFF_PERCENTAGE` - means the total of the cart will be discounted with the provided percentage value

**CartItem.js**

The cart should be able to handle product level discount too. The skeleton for this is in the `CartItem.js`.
Should support the following discount types:
- `VALUE_OFF` - means the price of the cart item will be discounted with the provided value
- `VALUE_OFF_PERCENTAGE` - means the price of the cart item will be discounted with the provided percentage value
- `CONDITIONAL_NEW_PRICE` - means if the cart contains X number of Y product the original price will be replaced with the one provided in the discount's `newPrice`.
