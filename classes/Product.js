class Product {
    constructor(id, name, price, discount) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.discount = discount;
    }
}

module.exports = Product;
