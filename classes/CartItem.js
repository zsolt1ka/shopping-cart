const d = require("./Discount");
const DiscountType = d.DiscountType;

class CartItem {
    constructor(product, count) {
        this.product = product;
        this.count = count;
    }

    /**
     * Calculates the price of the cart item, considering the product's price, product count and product level discount
     *
     * @returns {number}
     */
    getPrice = () => {
        if (this.product.discount) {
            switch (this.product.discount.type) {
                case DiscountType.VALUE_OFF:
                   // TODO: implement me
                case DiscountType.VALUE_OFF_PERCENTAGE:
                // TODO: implement me
                case DiscountType.CONDITIONAL_NEW_PRICE:
                    // TODO: implement me
                default:
                    return this.product.price * this.count;
            }
        }

        return this.product.price * this.count;
    }
}

module.exports = CartItem;
