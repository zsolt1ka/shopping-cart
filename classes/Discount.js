const DiscountType = {
    VALUE_OFF: 'value_off',
    VALUE_OFF_PERCENTAGE: 'value_off_percentage',
    CONDITIONAL_NEW_PRICE: 'conditional_new_price'
}

class Discount {
    constructor(type, offValue, newPrice, newPriceCountCondition) {
        this.type = type;
        this.offValue = offValue;
        this.newPrice = newPrice;
        this.newPriceCountCondition = newPriceCountCondition;
    }
}

module.exports = {
    Discount,
    DiscountType
};
