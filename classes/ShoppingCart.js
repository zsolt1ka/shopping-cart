const CartItem = require("./CartItem");

const d = require("./Discount");
const DiscountType = d.DiscountType;

class ShoppingCart {
    constructor() {
        this.cartItems = [];
    }

    /**
     * Creates a CartItem and adds it to the cart items using the Product and the count
     * If the Product already exist in the cart increases the count of the CartItem
     *
     * @param product
     * @param count
     */
    addProduct = (product, count) => {
        // TODO: implement me
    }

    /**
     * Changes the Product count in the CartItem
     *
     * @param product
     * @param count
     */
    updateCount = (product, count) => {
        // TODO: implement me
    }

    /**
     * Removes the Product and the cart item from the cart
     *
     * @param item
     */
    removeProduct = (item) => {
        // TODO: implement me
    }

    /**
     * Calculates the cart total considering the discount
     *
     * @returns {number}
     */
    getTotal = () => {
        const total = this.cartItems.reduce((prev, cartItem) => {
            return prev + cartItem.getPrice()
        }, 0);
        return parseFloat(this.applyDiscount(total).toFixed(2));
    }

    /**
     * Applies a discount if is set on the cart's total
     *
     * @param total
     * @returns {number|*}
     */
    applyDiscount = (total) => {
        if (this.discount) {
            switch (this.discount.type) {
                case DiscountType.VALUE_OFF:
                // TODO: implement me
                case DiscountType.VALUE_OFF_PERCENTAGE:
                // TODO: implement me
                default: return total;
            }
        }

        return total;
    }

    setDiscount = (discount) => {
        this.discount = discount;
    }

    itemCount = () => this.cartItems.length;

    getItem = (index) => {
        if (!this.cartItems.length || this.cartItems.length < index) {
            return null;
        }

        return this.cartItems[index];
    }

    reset = () => {
        this.cartItems = [];
        this.discount = null;
    }
}

module.exports = ShoppingCart;
